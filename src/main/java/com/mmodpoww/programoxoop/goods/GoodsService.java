/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mmodpoww.programoxoop.goods;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Acer
 */
public class GoodsService {
    private static ArrayList<Goods> goodsList = goodsList = new ArrayList<>();
    private static int countprice = 0;
    static {
        //mock data
//        goodsList.add(new Goods("001","IPadAir","Apple",10000,10));
//        goodsList.add(new Goods("002","IPhone ","Apple",20000,10));
//        goodsList.add(new Goods("003","AirPod ","Apple",50000,10));
    }
    public static boolean addGoods(Goods goods){
        goodsList.add(goods);
        return true;
    }
    public static boolean updateGoods(int index,Goods goods){
        goodsList.set(index, goods);
        return true;
    }
    public static boolean delGoods(Goods goods){
        goodsList.remove(goods);
        return true;
    }
    public static boolean clearGoods(int index){
        goodsList.removeAll(goodsList);
        return true;
    }
    public static ArrayList<Goods> getGoods() {
        return goodsList;
    }
    public static Goods getGoods(int index) {
        return goodsList.get(index);
    }
    public static boolean delGoodsIndex(int index) {
        goodsList.remove(index);
        return true;
    }
//    public static boolean totalPrice(Goods goods){
//        goodsList.
//        return true;
//    }
    public static void save() {
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            file = new File("mmodpoww.dat");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(goodsList);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GoodsService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(GoodsService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public static void load() {
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            file = new File("mmodpoww.dat");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            goodsList = (ArrayList<Goods>) ois.readObject();
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GoodsService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(GoodsService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(GoodsService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
